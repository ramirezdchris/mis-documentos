-- drop database bdd_hotel_mandy;

create database if not exists bdd_hotel_mandy;

use bdd_hotel_mandy;

create table if not exists imagenes(
	id_imagen int not null primary key auto_increment,
	url varchar(1000) not null
)Engine InnoDB;

insert into imagenes (url) values ("asd"), ("dsa");

select * from imagenes;

delete from imagenes where id_imagen > 6;

create table if not exists informaciones(
	id_informacion int not null primary key auto_increment,
	informacion varchar(150) not null
)Engine InnoDB;

create table if not exists tipo_habitaciones(
	id_tipo_habitacion int not null primary key auto_increment,
	nombre varchar(50) not null,
	descripcion varchar(100) not null
)Engine InnoDB;

create table if not exists informaciones_imagenes(
	id_informacion_imagen int not null primary key auto_increment,
    id_imagen int not null,
    id_informacion int not null,
    constraint foreign key (id_imagen) references imagenes(id_imagen) 
		on delete cascade on update cascade,
    constraint foreign key (id_informacion) references informaciones(id_informacion) 
		on delete cascade on update cascade
)Engine InnoDB;

create table if not exists habitaciones(
	id_habitacion int not null primary key auto_increment,
	numero_habitacion varchar(5) not null,
	piso int not null,
	precio double not null,
    cama int not null,
	id_tipo_habitacion int not null,
    id_informacion_imagen int not null,
    constraint q foreign key (id_tipo_habitacion) references tipo_habitaciones(id_tipo_habitacion)
		on delete cascade on update cascade,
	constraint foreign key (id_informacion_imagen) references informaciones_imagenes(id_informacion_imagen) 
		on delete cascade on update cascade
)Engine InnoDB;



create table if not exists huespedes(
	id_huesped int not null primary key auto_increment,
	nombre varchar(50) not null,
    usuario varchar(50) not null,
	apellido varchar(50) not null,
	edad int not null not null,
	direccion varchar(150) not null,
	telefono varchar(9) not null,
	email varchar(150) not null,
    contrasena varchar(130) not null
)Engine InnoDB;

create table if not exists reservas(
	id_reserva int not null primary key auto_increment,
	fecha_ingreso date not null,
	fecha_salida date not null,
	costo_alojamiento decimal not null,
    estado_reserva int not null,
	id_huesped int not null,
	id_habitacion int not null,
    constraint foreign key (id_huesped) references huespedes(id_huesped) 
		on delete cascade on update cascade,
    constraint foreign key (id_habitacion) references habitaciones(id_habitacion) 
		on delete cascade on update cascade
)Engine InnoDB;

create table if not exists comentarios(
	id_comentario int not null primary key auto_increment,
    fecha date not null,
    hora varchar(10) not null,
    id_huesped int not null,
    constraint t foreign key (id_huesped) references huespedes(id_huesped)
		on delete cascade on update cascade
)Engine InnoDB;

create table if not exists usuarios(
	id_usuario int not null primary key auto_increment,
	usuario varchar(50) not null,
	contrasena varchar(130) not null,
	email varchar(100) not null,
	telefono varchar (30),
	rol int not null
)Engine InnoDB;


INSERT INTO tipo_habitaciones VALUES(0,'Presidencial','Ejemplo1'),(0,'Rey','Ejemplo2'),(0,'Princesa','Ejemplo3');


INSERT INTO informaciones VALUES(0,'blablabla'),(0,'blebleble'),(0,'bliblibli');

INSERT INTO informaciones_imagenes VALUES(0,1,1),(0,1,2);


INSERT INTO habitaciones VALUES (0,'01',1,25.00,2,3,1),
								(0,'02',1,30.00,3,2,1),
								(0,'03',1,25.00,2,3,1),
                                (0,'04',1,45.00,4,2,2),
                                (0,'05',1,50.00,6,1,2),
                                (0,'06',1,25.00,2,3,2);
                                
                                
                                
INSERT INTO huespedes VALUES(0,'Christian','chris','Sarmiento',22,'Colonia USAM','2257-7777','chris@gmail.com','123'),
							(0,'Kevin','kevin','Santiago',20,'Colonia UTEC','2257-7777','kevin@gmail.com','123'),
                            (0,'Juan','juan','Nativi',24,'Colonia GAVIDIA','2257-7777','juan@gmail.com','123');

-- Numero Habitaciones
SELECT COUNT(*) AS cupos  FROM habitaciones;


SELECT * FROM habitaciones;
SELECT * FROM huespedes;
SELECT * FROM reservas;

INSERT INTO reservas VALUES(0,'2020-01-25','2020-01-28',25.00,0,1,2);


DELETE FROM reservas WHERE id_reserva = 6;
SELECT * FROM reservas WHERE fecha_ingreso = '2020-01-25';



SELECT (COUNT(habi.id_habitacion))+(COUNT(reser.id_habitacion)) AS Cupo, habi.numero_habitacion FROM reservas reser INNER JOIN
habitaciones habi ON habi.id_habitacion = reser.id_habitacion
WHERE EXISTS (SELECT 1 FROM reservas WHERE reser.id_habitacion = habi.id_habitacion) 
AND reser.fecha_ingreso >= '2020-01-25' AND reser.fecha_salida <= '2020-01-28';

SELECT COUNT(*) FROM reservas reser
INNER JOIN habitaciones habi ON habi.id_habitacion = reser.id_habitacion
WHERE EXISTS(SELECT 1 FROM reservas WHERE reser.id_habitacion = habi.id_habitacion)
AND reser.fecha_salida > '20-01-27';

SELECT COUNT(*) FROM reservas reser 
WHERE reser.fecha_salida > '2020-01-28';

DROP PROCEDURE sp_consulta;
/*
DELIMITER //
CREATE PROCEDURE sp_consulta(IN fecha_ingreso DATE, IN fecha_salida DATE, IN estadore INT)
BEGIN    
	DECLARE ocupadas INT;
    SET @ocupadas = (SELECT COUNT(*) FROM reservas reser 
    WHERE (fecha_salida <= reser.fecha_salida  OR fecha_ingreso >= reser.fecha_ingreso) AND estado_reserva = estadore);
    
    SELECT COUNT(id_habitacion) - @ocupadas AS Disponibles,th.nombre  FROM habitaciones h 
    INNER JOIN tipo_habitaciones th ON th.id_tipo_habitacion = h.id_tipo_habitacion
    GROUP BY th.id_tipo_habitacion;
    
END//
DELIMITER //;
*/

SELECT * FROM reservas WHERE fecha_salida = '2020-01-27';
CALL sp_consulta('2020-01-20','2020-01-21', 0);

CALL sp_consultaHabi('2020-01-25','2020-01-28', 0);

SELECT * FROM reservas;
CALL sp_consultaHabiTipo('2020-01-25','2020-01-28', 1);


-- 
SELECT COUNT(*) FROM habitaciones
GROUP BY id_tipo_habitacion;



DELIMITER //
CREATE PROCEDURE sp_consultaHabi(IN fecha_ingreso DATE, IN fecha_salida DATE, IN estadore INT)
BEGIN
	DECLARE ocupadas INT;
    SET @ocupadas = (SELECT COUNT(*) FROM reservas reser 
    WHERE ((fecha_ingreso BETWEEN reser.fecha_ingreso AND reser.fecha_salida) OR (fecha_salida BETWEEN reser.fecha_ingreso AND reser.fecha_salida)) AND estado_reserva = estadore);
    
SELECT 
    COUNT(id_habitacion) - @ocupadas AS Disponibles, th.nombre
FROM
    habitaciones h
        INNER JOIN
    tipo_habitaciones th ON th.id_tipo_habitacion = h.id_tipo_habitacion
GROUP BY th.id_tipo_habitacion;
END //
DELIMITER //;


-- Total de habitaciones de ID tipo 1
SELECT COUNT(th.id_tipo_habitacion) FROM habitaciones h 
INNER JOIN tipo_habitaciones th ON th.id_tipo_habitacion = h.id_tipo_habitacion
WHERE th.id_tipo_habitacion = 1
GROUP BY th.id_tipo_habitacion;

-- Total de habitaciones de ID tipo 2
SELECT COUNT(th.id_tipo_habitacion) FROM habitaciones h 
INNER JOIN tipo_habitaciones th ON th.id_tipo_habitacion = h.id_tipo_habitacion
WHERE th.id_tipo_habitacion = 2
GROUP BY th.id_tipo_habitacion;

-- Total de habitaciones de ID tipo 3
SELECT COUNT(th.id_tipo_habitacion) FROM habitaciones h 
INNER JOIN tipo_habitaciones th ON th.id_tipo_habitacion = h.id_tipo_habitacion
WHERE th.id_tipo_habitacion = 3
GROUP BY th.id_tipo_habitacion;

DROP PROCEDURE sp_consultaHabiTipo;
DELIMITER //
CREATE PROCEDURE sp_consultaHabiTipo(IN fecha_ingreso DATE, IN fecha_salida DATE, IN habitacion INT)
BEGIN
	-- DECLARE ocupadas INT;
	-- SET @ocupadas = (SELECT COUNT(*) FROM reservas reser 
    -- WHERE ((fecha_ingreso BETWEEN reser.fecha_ingreso AND reser.fecha_salida) OR (fecha_salida BETWEEN reser.fecha_ingreso AND reser.fecha_salida)) AND estado_reserva = estadore);
    
    SELECT th.nombre, h.id_habitacion  FROM habitaciones h 
    INNER JOIN tipo_habitaciones th ON th.id_tipo_habitacion = h.id_tipo_habitacion
    INNER JOIN reservas reser ON reser.id_habitacion = h.id_habitacion
    WHERE ((fecha_ingreso BETWEEN reser.fecha_ingreso AND reser.fecha_salida) OR (fecha_salida BETWEEN reser.fecha_ingreso AND reser.fecha_salida))
    AND h.id_habitacion = habitacion;
    -- GROUP BY th.id_tipo_habitacion;
END //
DELIMITER //;
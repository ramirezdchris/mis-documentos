drop database if exists competit;
create database competit;
use competit;

create table usuarios(
usuario_id int not null,
nombre varchar(20) not null,
carnet varchar(5) not null,
constraint pk_usuarios primary key (usuario_id)
);

create table competencias(
competencia_id int not null,
nombre varchar(30) not null,
descripcion varchar(50) not null,
us_id  int not null,
constraint pk_competencias primary key (competencia_id),
constraint fk_competencias foreign key (us_id) references usuarios(usuario_id)
);